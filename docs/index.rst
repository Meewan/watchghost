Welcome to WatchGhost's documentation
=====================================

Watchghost is a simple to install and simple to configure monitoring server. It
is also a modern Python application making great use of asynchronous
architecture.

You have services? You need to monitor them? You don't want to lose your time
on configuring a suuuper boring and complex monitoring system? And you don't
want to install additionals services on your server? Watchghost is for you!

Configuring your little Watchghost is super easy and quick! Only 4 tiny cute
little files: groups, loggers, watchers, servers. You only need to put your
server(s) on the servers config file, define how you want to watch them with
the watchers file, and how you want to be alerted in the loggers file. If you
have some spare time, you can even group your servers on the groups config
file, so you can monitor a whole group in one config line!

Contents:

.. toctree::
   :maxdepth: 3

   install
   deploy
   configuration
   loggers
   watchers
   changelog
